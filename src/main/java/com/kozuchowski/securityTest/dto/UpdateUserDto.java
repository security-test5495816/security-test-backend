package com.kozuchowski.securityTest.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;


@AllArgsConstructor
@Setter
@Getter
public class UpdateUserDto {

    @NotBlank(message = "Id is mandatory")
    private Long id;
    @NotBlank(message = "Name is mandatory")
    private String name;
    @NotBlank(message = "Role is mandatory")
    private String role;
}
