package com.kozuchowski.securityTest.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;


@Getter
@Setter
@AllArgsConstructor
public class CreateUserDto {
    @NotBlank(message = "Name is mandatory")
    private String name;
    @NotBlank(message = "Role is mandatory")
    private String role;
}
