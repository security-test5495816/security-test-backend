package com.kozuchowski.securityTest.service;

import com.kozuchowski.securityTest.dto.CreateUserDto;
import com.kozuchowski.securityTest.dto.UpdateUserDto;
import com.kozuchowski.securityTest.model.User;
import com.kozuchowski.securityTest.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.rmi.NoSuchObjectException;
import java.util.Optional;

@Service
public class UserService {


    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User create(CreateUserDto dto) {
        User user = new User();
        user.setName(dto.getName());
        user.setRole(dto.getRole());

        return userRepository.save(user);
    }

    public User read(Long id) throws NoSuchObjectException {
        Optional<User> optionalUser = userRepository.findById(id);

        if(optionalUser.isEmpty()) {
            throw new NoSuchObjectException("No such user");
        }

        return optionalUser.get();
    }


    public User update(UpdateUserDto dto) throws NoSuchObjectException {
        Optional<User> optionalUser = userRepository.findById(dto.getId());

        if(optionalUser.isEmpty()) {
            throw new NoSuchObjectException("No such user");
        }
        User user = optionalUser.get();

        user.setName(dto.getName());
        user.setRole(dto.getRole());

         return userRepository.save(user);
    }
    public String delete(Long id) {
        userRepository.deleteById(id);
        return "Deleted";
    }


}
