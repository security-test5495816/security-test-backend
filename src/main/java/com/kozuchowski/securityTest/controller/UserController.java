package com.kozuchowski.securityTest.controller;

import com.kozuchowski.securityTest.dto.CreateUserDto;
import com.kozuchowski.securityTest.dto.UpdateUserDto;
import com.kozuchowski.securityTest.model.User;
import com.kozuchowski.securityTest.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;


import java.rmi.NoSuchObjectException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    @PostMapping("/")
    public User create(@Valid @RequestBody CreateUserDto dto) {
        return userService.create(dto);
    }

    @GetMapping("/{id}")
    public User read(@PathVariable Long id) throws NoSuchObjectException {
        return userService.read(id);
    }

    @PatchMapping("/")
    public User update(@Valid UpdateUserDto dto) throws NoSuchObjectException {
        return  userService.update(dto);
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable Long id) {
        return userService.delete(id);

    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
