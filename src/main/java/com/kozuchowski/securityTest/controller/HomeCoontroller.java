package com.kozuchowski.securityTest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class HomeCoontroller {

    @GetMapping("/everyone")
    public String all(){
        return "Hello everyone!";
    }

    @GetMapping("/admin")
    public String admin() {
        return "Hello admin!";
    }
}
